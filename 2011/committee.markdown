## Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alexandre Ferreira, IBM Research Labs, United States
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Almir Guimarães, Universidade Federal de Alagoas, Brazil
* André Detsch, Wellcare Automação Ltda., Brazil
* Andre Du Bois, Universidade Federal de Pelotas, Brazil
* Andre Gradvohl, Universidade Estadual de Campinas, Brazil
* Andrea Charao, Universidade Federal de Santa Maria, Brazil
* Antônio Augusto Fröhlich, Universidade Federal de Santa Catarina, Brazil
* Autran Macêdo, Universidade Federal de Uberlândia, Brazil
* Bruno Schulze, Laboratório Nacional de Computação Científica, Brazil
* Carla Lima Reis, Universidade Federal do Pará, Brazil
* Carlos Santos Jr., University of Nottingham, United Kingdom
* Carlos Soares Neto, Pontifícia Universidade Católica do Rio de Janeiro, Brazil
* Claudio de Sá, Universidade do Extremo Sul Catarinense, Brazil
* Cristiana Bentes, Universidade do Estado do Rio de Janeiro, Brazil
* Cristiano Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Daniel Batista, Universidade de São Paulo, Brazil
* Daniel Weingaertner, Universidade Federal do Paraná, Brazil
* Edgard Correa, Universidade Federal do Rio Grande do Norte, Brazil
* Eduardo Figueiredo, Universidade Federal de Minas Gerais, Brazil
* Eduardo Todt, Universidade Federal do Paraná, Brazil
* Fabiano Silva, Universidade Federal do Paraná, Brazil
* Fabio Costa, Universidade Federal de Goiás, Brazil
* Fernando Osório, Universidade de São Paulo, Brazil
* Frank Siqueira, Universidade Federal de Santa Catarina, Brazil
* Gaspare Bruno, Universidade Federal do Rio de Janeiro, Brazil
* Gerson Geraldo H. Cavalheiro, Universidade Federal de Pelotas, Brazil
* Giovanni Barroso, Universidade Federal do Ceará, Brazil
* Hisham Muhammad, GoboLinux.org, Brazil
* Iwens Sene Jr, Universidade Federal de Goiás, Brazil
* João Netto, Universidade Federal do Rio Grande do Sul, Brazil
* José Maldonado, Universidade de São Paulo, Brazil
* José Eduardo De Lucca, Universidade Federal de Santa Catarina, Brazil
* Jussara Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Lau Cheuk Lung, Universidade Federal de Santa Catarina, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Lucas Ferrari de Oliveira, Universidade Federal do Paraná, Brazil
* Luis Carlos De Bona, Universidade Federal do Paraná, Brazil
* Marcelo Finger, Universidade de São Paulo, Brazil
* Marcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marcio Moreno, Pontifícia Universidade Católica do Rio de Janeiro, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marcos Sunye, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Federal de Pelotas, Brazil
* Marluce Pereira, Universidade Federal de Lavras, Brazil
* Mauricio Pilla, Universidade Federal de Pelotas, Brazil
* Maximiliano Faria, Universidade Federal do Estado do Rio de Janeiro, Brazil
* Patricia Kayser Vargas Mangan, Centro Universitário La Salle, Brazil
* Paulo Maciel, Universidade Federal de Pernambuco, Brazil
* Paulyne Jucá, Universidade Federal de Pernambuco, Brazil
* Rafael dos Santos, CEITEC S.A., Brazil
* Regina Araujo, Universidade Federal de São Carlos, Brazil
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Renan Cattelan, Universidade Federal de Uberlândia, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Ricardo Ferrari, Universidade Federal de São Carlos, Brazil
* Rivalino Matias Jr., Universidade Federal de Uberlândia, Brazil
* Rodrigo Araújo Real, Freedom Veículos Elétricos Ltda., Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Sílvio Cazella, Universidade do Vale do Rio dos Sinos, Brazil
* Simone A. da Costa Cavalheiro, Universidade Federal de Pelotas, Brazil
* Wagner Meira Jr., Universidade Federal de Minas Gerais, Brazil
* Weverton Luis da Costa Cordeiro, Universidade Federal do Rio Grande do Sul, Brazil

## Organizing Committee

General Chair

* Simone André da Costa Cavalheiro (Universidade Federal de Pelotas, Brazil)

Program Committee Chair

* Rivalino Matias Jr. (Universidade Federal de Uberlândia, Brazil)
