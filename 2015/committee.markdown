## Program Committee

* Alfredo Goldman, Universidade de São Paulo, Brazil
* Antônio Terceiro, COLIVRE/Universidade de Brasília, Brazil
* Carlos  Denner dos Santos, Universidade de Brasília, Brazil
* Christina Chavez, Universidade Federal da Bahia, Brazil
* Eduardo Guerra, Instituto Nacional de Pesquisas Espaciais, Brazil
* Fabio Kon, Universidade de São Paulo, Brazil
* Filipe Saraiva, Universidade de São Paulo, Brazil
* Gregorio Robles, Universidad Rey Juan Carlos, Spain
* Islene Calciolari Garcia, Universidade Estadual de Campinas, Brazil
* Jesus Gonzalez-Barahona, Universidad Rey Juan Carlos, Spain
* Joaquim Uchôa, Universidade Federal de Lavras, Brazil
* Kevin Crowston, Syracuse University School of Information Studies, United States
* Klaas-Jan Stol, Lero - the Irish Software Engineering Research Centre, University of Limerick, Ireland
* Luis Felipe Murillo, University of California, United States
* Marcelo Finger, Universidade de São Paulo, Brazil
* Marco Aurélio Graciotto Silva, Universidade Tecnológica Federal do Paraná, Brazil
* Marco Gerosa, Universidade de São Paulo, Brazil
* Nelson Pretto, Universidade Federal da Bahia, Brazil
* Paulo Meirelles, Universidade de Brasília, Brazil
* Rafael Prikladnicki, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Reginaldo Ré, Universidade Tecnológica Federal do Paraná, Brazil
* Roberto Bittencourt, Universidade Estadual de Feira de Santana, Brazil
* Sandro Morasca, Università degli Studi dell'Insubria, Italy
* Scott Hissam, Carnegie Mellon University, United States

## Organizing Committee

General Chair:

* Igor Steinmacher (Universidade Tecnológica Federal do Paraná, Brazil)

Program Committee Chair:

* Sandro S. Andrade (Instituto Federal de Educação, Ciência e Tecnologia da Bahia, Brazil)
