## Program Committee

* Benhur Stein, Universidade Federal de Santa Maria, Brazil
* Denise Bandeira da Silva, Universidade do Vale do Rio dos Sinos, Brazil
* Fernando Santos Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Francisco Assis Moreira do Nascimento, Universidade Luterana do Brasil, Brazil
* Gilberto Fernandes Marchioro, Universidade Luterana do Brasil, Brazil
* Ivan Saraiva Silva, Universidade Federal do Rio Grande do Norte, Brazil
* Joao Batista Oliveira, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul/Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Luis Fernando Fortes Garcia, Universidade Feevale, Brazil
* Marinho Barcellos, Universidade do Vale do Rio dos Sinos, Brazil
* Mouriac Hallen Diemer, Unidade Integrada Vale do Taquari de Ensino Superior, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Taisy Silva Weber, Universidade Federal do Rio Grande do Sul, Brazil

## Organizing Committee

General Chair

* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)

Program Committee Chair

* Benhur Stein (Universidade Federal de Santa Maria, Brazil)

Organizing Committee

* César Augusto Fonticielha De Rose (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Denise Bandeira da Silva (Universidade do Vale do Rio dos Sinos, Brazil)
* Douglas Kellermann (Universidade Feevale, Brazil)
* Luciana Porcher Nedel (Universidade Federal do Rio Grande do Sul/Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
