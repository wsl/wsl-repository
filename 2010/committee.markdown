## Program Committee

* Adenauer Corrêa Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alba melo, Universidade de Brasília, Brazil
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Alfredo Goldman, Universidade de São Paulo, Brazil
* André Detsch, Universidade do Vale do Rio dos Sinos, Brazil
* André Du Bois, Universidade Federal de Pelotas, Brazil
* André Gradvohl, Universidade São Francisco, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Autran Macêdo, Universidade Federal de Uberlândia, Brazil
* Benhur de Oliveira Stein, Universidade Federal de Santa Maria, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Paraná, Brazil
* Claudio de Sá, Universidade do Extremo Sul Catarinense, Brazil
* Cristiano Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Daniel Weingaertner, Universidade Federal do Paraná, Brazil
* Eduardo Todt, Universidade Federal do Paraná, Brazil
* Ewerton Longoni Madruga, Instituto Nacional de Metrologia, Qualidade e Tecnologia, Brazil
* Fabiano Silva, Universidade Federal do Paraná, Brazil
* Fernando Osório, Universidade de São Paulo, Brazil
* Gaspare Bruno, Centro Universitário La Salle, Brazil
* Gerson Geraldo H. Cavalheiro, Universidade Federal de Pelotas, Brazil
* Hisham Muhammad, GoboLinux.org, Brazil
* João Netto, Universidade Federal do Rio Grande do Sul, Brazil
* José Maldonado, Universidade de São Paulo, Brazil
* José Ramos Gonçalves, Universidade Federal do Ceará, Brazil
* Jussara Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Lau Cheuk Lung, Universidade Federal de Santa Catarina, Brazil
* Leonardo Lemes Fagundes, Universidade do Vale do Rio dos Sinos, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Lucas Ferrari de Oliveira, Universidade Federal do Paraná, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Luis Carlos De Bona, Universidade Federal do Paraná, Brazil
* Marcelo Finger, Universidade de São Paulo, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marcos Sunye, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Federal de Pelotas, Brazil
* Patricia Kayser Vargas Mangan, Centro Universitário La Salle, Brazil
* Paulyne Jucá, Universidade Federal de Pernambuco, Brazil
* Regina Araujo, Universidade Federal de São Carlos, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Rodrigo Araújo Real, Freedom Veículos Elétricos Ltda., Brazil
* Ronaldo Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Ronaldo Fernandes Ramos, Instituto Federal de Educação, Ciência e Tecnologia do Ceará, Brazil
* Silvio Cazella, Universidade do Vale do Rio dos Sinos, Brazil
* Simone A. da Costa, Universidade Federal de Pelotas, Brazil

## Organizing Committee

General Chair

* Cristiano Costa (Universidade do Vale do Rio dos Sinos, Brazil)

Program Committee Chair

* Eduardo Todt (Universidade Federal do Paraná, Brazil)
