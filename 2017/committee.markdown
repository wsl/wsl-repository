## Program Committee

* Alfredo Goldman, Universidade de São Paulo, Brazil
* Ana Cristina Fricke Matte, Universidade Federal de Minas Gerais, Brazil
* André Luis Azevedo Guedes, RODA Consultoria e Treinamento, Brazil
* Antonio Terceiro, COLIVRE, Brazil
* Aracele Torres, Universidade de São Paulo, Brazil
* Bernardo Estácio, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Carlos Denner dos Santos Jr., Universidade de Brasília, Brazil
* Cíntia Inês Boll, Universidade Federal do Rio Grande do Sul, Brazil
* Christina Chavez, Universidade Federal da Bahia, Brazil
* Daniel Batista, Universidade de São Paulo, Brazil
* Eduardo Guerra, Instituto Nacional de Pesquisas Espaciais, Brazil
* Filipe Saraiva, Universidade Federal do Pará, Brazil
* Francisco José Monaco, Universidade de São Paulo, Brazil
* Gregorio Robles, Universidad Rey Juan Carlos, Spain
* Igor Steinmacher, Universidade Tecnológica Federal do Paraná, Brazil
* Igor Wiese, Universidade Tecnológica Federal do Paraná, Brazil
* Islene Calciolari Garcia, Universidade Estadual de Campinas, Brazil
* Jéferson Campos Nobre, Universidade do Vale do Rio dos Sinos, Brazil
* José Carlos Maldonado, Universidade de São Paulo, Brazil
* Lucas Mello Schnorr, Universidade Federal do Rio Grande do Sul, Brazil
* Lucio Agostinho Rocha, Universidade Tecnológica Federal do Paraná, Brazil
* Marco Aurélio Graciotto Silva, Universidade Tecnológica Federal do Paraná, Brazil
* Paulo Meirelles, Universidade de Brasília, Brazil
* Rafael Evangelista, Universidade Estadual de Campinas, Brazil
* Rodrigo Rocha Gomes e Souza, Universidade Federal da Bahia, Brazil
* Sandro Morasca, Università degli Studi dell'Insubria, Italy

## Organizing Committee

General Chairs:

* Claudio Fernando Resin Geyer (Universidade Federal do Rio Grande do Sul, Brazil)
* Daniel Nehme Müller (Conexum/Faculdade Monteiro Lobato, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil/Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)

