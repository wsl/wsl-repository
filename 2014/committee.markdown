## Program Committee

* Antônio Terceiro, Linaro, Brazil
* Carlos  Denner Dos Santos, Universidade de Brasília, Brazil
* Christina Chavez, Universidade Federal da Bahia, Brazil
* Eduardo Guerra, Instituto Nacional de Pesquisas Espaciais, Brazil
* Gregorio Robles, Universidad Rey Juan Carlos, Spain
* José Carlos Maldonado, ICMC/Universidade de São Paulo, Brazil
* Klaas-Jan Stol, Lero - the Irish Software Engineering Research Centre, University of Limerick, Ireland
* Manoel Mendonça, Universidade Federal da Bahia, Brazil
* Marcelo Finger, IME/Universidade de São Paulo, Brazil
* Marco Aurélio Graciotto Silva, Universidade Tecnológica Federal do Paraná, Brazil
* Marco Tulio Valente, Universidade Federal de Minas Gerais, Brazil
* Megan Squire, Elon University, USA
* Paulo Meirelles, Universidade de Brasília, Brazil
* Reginaldo Ré, Universidade Tecnológica Federal do Paraná, Brazil
* Roberto Bittencourt, Universidade Estadual de Feira de Santana, Brazil
* Sandro Morasca, Università degli Studi dell'Insubria, Italy
* Scott Hissam, SEI/Carnegie Mellon University, USA

## Organizing Committee

General Co-Chairs:

* Igor Steinmacher (Universidade Tecnológica Federal do Paraná, Brazil)
* Marco A. Graciotto Silva (Universidade Tecnológica Federal do Paraná, Brazil)

Program Committee Chair:

* Rodrigo Souza (Universidade Federal da Bahia, Brazil)
