## National and International Track Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Alfredo Goldman, Universidade de São Paulo, Brazil
* André Detsch, IBM, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Carla Alessandra Lima Reis, Universidade Federal do Pará, Brazil
* Carlos Campani, Universidade Federal de Pelotas, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Paraná, Brazil
* Cesar De Rose, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Cristiana Bentes, Universidade do Estado do Rio de Janeiro, Brazil
* Cristiano André da Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Ewerton Longoni Madruga, Instituto Nacional de Metrologia, Qualidade e Tecnologia, Brazil
* Fernando Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Gerson Cavalheiro, Universidade Federal de Pelotas, Brazil
* Hisham Hashem Muhammad, Pontifícia Universidade Católica do Rio de Janeiro, Brazil
* Iara Augustin, Universidade Federal de Santa Maria, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* Jorge Barbosa, Universidade do Vale do Rio dos Sinos, Brazil
* Jussara Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Leonardo Lemes Fagundes, Universidade do Vale do Rio dos Sinos, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Márcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Católica de Pelotas, Brazil
* Marinho Pilla Barcellos, Universidade do Vale do Rio dos Sinos, Brazil
* Mario Domenech Goulart, Universidade Federal do Rio Grande do Sul, Brazil
* Mauricio Lima Pilla, Universidade Católica de Pelotas, Brazil
* Ney Lemke, Universidade Estadual Paulista, Brazil
* Nicolas Maillard, Universidade Federal do Rio Grande do Sul, Brazil
* Rafael dos Santos, Universidade de Santa Cruz do Sul, Brazil
* Rômulo Silva de Oliveira, Universidade Federal de Santa Catarina, Brazil
* Regina Borges de Araujo, Universidade Federal de São Carlos, Brazil
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Renata Galante, Universidade Federal do Rio Grande do Sul, Brazil
* Rodrigo Araújo Real, Universidade Federal do Rio Grande do Sul, Brazil
* Rodrigo Quites Reis, Universidade Federal do Pará, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Wagner Meira Jr., Universidade Federal de Minas Gerais, Brazil

## Organizing Committee

General Chair

* Marinho Pilla Barcellos (Universidade do Vale do Rio dos Sinos, Brazil)

Organizing Committee

* Gerson Cavalheiro (Universidade Federal de Pelotas, Brazil)
* Nicolas Maillard (Universidade Federal do Rio Grande do Sul, Brazil)

National Track Program Committee Chair

* Gerson Cavalheiro (Universidade Federal de Pelotas, Brazil)

International Track Program Committee Chair

* Nicolas Maillard (Universidade Federal do Rio Grande do Sul, Brazil)

Free Software in University Track Program Committee Chair

* Javam Machado (Universidade Federal do Ceará, Brazil)
